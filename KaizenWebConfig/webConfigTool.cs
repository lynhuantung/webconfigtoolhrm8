﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;//Sử dụng thư viện này để làm việc với Stream
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Configuration;
using System.Security.Cryptography;
using VnResource.Helper.Data;

namespace KaizenWebConfig
{
    public partial class webConfigTool : Form
    {
        private static bool isConnectSuccess = false;
        public webConfigTool()
        {
            InitializeComponent();
        }

        public  string MD5Hash(string input)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }

        private void btnCapNhatConfig_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtDuongDan.Text) || string.IsNullOrEmpty(txtUser.Text) ||
                string.IsNullOrEmpty(txtDBName.Text) || string.IsNullOrEmpty(txtDiaChi.Text)
                )
            {
                MessageBox.Show("Vui lòng nhập đầy đủ thông tin", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {

                try
                {
                    #region Khai báo
                    var duongDan = txtDuongDan.Text;
                    duongDan = duongDan.Replace("\\", "\\");
                    var webConfigMain = duongDan + "\\Presentation\\HRM.Presentation.Main\\Web.config";
                    var webConfigHrService = duongDan + "\\Presentation\\HRM.Presentation.Hr.Service\\Web.config";
                    var webConfigSysService = duongDan + "\\Presentation\\HRM.Presentation.HrmSystem.Service\\Web.config";
                    var webConfigPortalService = duongDan + "\\Presentation\\HRM.Presentation.EmpPortal\\Web.config";
                    var serverName = txtDiaChi.Text.Trim();
                    var dbName = txtDBName.Text.Trim();
                    var userName = txtUser.Text.Trim();
                    var password = txtPassword.Text.Encrypt();
                    #endregion

                    #region Check Connecting
                    var checkConnect = SQLOpenConnection(txtDiaChi.Text, txtDBName.Text, txtUser.Text, txtPassword.Text);
                    if (!checkConnect)
                    {
                        MessageBox.Show("Kết nối SQL thất bại, vui lòng kiểm tra lại dữ liệu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    #endregion

                    var configMainObj = OpenConfigFile(webConfigMain);//main
                    var configHRServiceObj = OpenConfigFile(webConfigHrService);//hr
                    var configSystemServiceObj = OpenConfigFile(webConfigSysService);//system
                    var configPortalObj = OpenConfigFile(webConfigPortalService);//portal

                    #region change Webconfig main
                    SetConfiguration(configMainObj, serverName, dbName, userName, password);
                    SetConfiguration(configMainObj, "IsAudit", "False");
                    SetConfiguration(configMainObj, "IsEncryptData", cbEncrypt.Checked.ToString());

                    #endregion

                    #region change Webconfig HR
                    SetConfiguration(configHRServiceObj, serverName, dbName, userName, password);
                    SetConfiguration(configHRServiceObj, "IsEncryptData", cbEncrypt.Checked.ToString());

                    #endregion

                    #region change Webconfig System
                    SetConfiguration(configSystemServiceObj, serverName, dbName, userName, password);
                    SetConfiguration(configSystemServiceObj, "IsEncryptData", cbEncrypt.Checked.ToString());

                    #endregion

                    #region change Webconfig Portal
                    if (cbEmpPortal.Checked)
                    {
                        SetConfiguration(configPortalObj, serverName, dbName, userName, password);
                    }
                    #endregion

                    MessageBox.Show("Cập nhật web config thành công", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }

        }

        private void btnChonDuongDan_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.ShowNewFolderButton = false;
            fbd.Description = "Chọn thư mục chứa Source";
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                txtDuongDan.Text = fbd.SelectedPath;
            }

        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            FormHuongDan formHD = new FormHuongDan();
            formHD.SetDesktopLocation(0, 0);
            formHD.ShowDialog();

        }



        #region common

        #region Kiểm tra kết nối
        private static bool SQLOpenConnection(string server, string dbName, string userID, string password)
        {
            var sqlconnectstr = string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3}", server, dbName, userID, password);
            try
            {
                SqlConnection SQLconnection = new SqlConnection(sqlconnectstr);
                SQLconnection.Open();
                SQLconnection.Close();
                isConnectSuccess = true;
            }
            catch (Exception)
            {
                isConnectSuccess = false;
            }
            return isConnectSuccess;
        }


        #endregion

        #region Get và set webconfig

        /// <summary>get config file</summary>
        /// <param name="configPath">đường dẫn webconfig (vd: d:\web.config)</param>
        /// <returns></returns>
        public static Configuration OpenConfigFile(string configPath)
        {
            var configFile = new FileInfo(configPath);
            var vdm = new VirtualDirectoryMapping(configFile.DirectoryName, true, configFile.Name);
            var wcfm = new WebConfigurationFileMap();
            wcfm.VirtualDirectories.Add("/", vdm);
            return WebConfigurationManager.OpenMappedWebConfiguration(wcfm, "/", "iis_website_name");
        }

        /// <summary>set key trong webconfig</summary>
        /// <param name="physicalPath">đường dẫn webconfig (vd: d:\web.config)</param>
        /// <param name="appSettingKey">key appsetting</param>
        /// <param name="appSettingValue">giá tri cua appsetting</param>
        /// <returns></returns>
        public static void SetConfiguration(Configuration configObj, string appSettingKey, string appSettingValue)
        {
            if (configObj == null || (configObj != null && configObj.AppSettings != null && configObj.AppSettings.Settings[appSettingKey] == null))
            {
                return;
            }

            FileInfo file = new FileInfo(configObj.FilePath);
            var isReadonlyInit = false;
            if (file != null && file.IsReadOnly)
            {
                file.IsReadOnly = false;
                isReadonlyInit = true;
            }

            

            configObj.AppSettings.Settings[appSettingKey].Value = appSettingValue;
            configObj.Save(ConfigurationSaveMode.Modified);

            if (isReadonlyInit && file != null)
            {
                file.IsReadOnly = true;
            }

        }

        /// <summary>set webconfig connectionString</summary>
        /// <param name="configObj"></param>
        /// <param name="serverName"></param>
        /// <param name="dataBaseName"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        public static void SetConfiguration(Configuration configObj, string serverName, string dataBaseName, string userName, string password)
        {
            if (configObj == null)
            {
                return;
            }

            FileInfo file = new FileInfo(configObj.FilePath);
            var isReadonlyInit = false;
            if (file != null && file.IsReadOnly)
            {
                file.IsReadOnly = false;
                isReadonlyInit = true;
            }

            #region set chuoi ket noi
            //doc chuoi ket noi                      
            var strConnectionString = string.Format(@"metadata=res://*/Entities.csdl|res://*/Entities.ssdl|res://*/Entities.msl;provider=System.Data.SqlClient;provider connection string=&quot;data source={0};initial catalog={1};user id={2};password={3};multipleactiveresultsets=True; persist security info=True;application name=EntityFramework&quot;", serverName, dataBaseName, userName, password);
            configObj.ConnectionStrings.ConnectionStrings["VnrConnectionString"].ConnectionString = strConnectionString;
            #endregion

            configObj.Save();


            if (isReadonlyInit && file != null)
            {
                file.IsReadOnly = true;
            }

        }

        #endregion

        #endregion

    }
}
